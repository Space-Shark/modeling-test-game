# Modeling Test - Space Shark Studios
---
First pass working with the following tools
* Maya 2016
* Photoshop 2015
* Substance Painter
* Unity 5.4.1
* Shader Forge

Just trying to make a good looking internal scene based on the Maya tutorial https://www.youtube.com/playlist?list=PLsPHRLf6UN4n778LjMnKVG1nw_PX8Lhja
